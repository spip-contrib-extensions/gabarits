<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'gabarits_description' => 'Gestion de gabarits (modèles de contenu) pour les éléments de SPIP.',
	'gabarits_slogan' => 'Des modèles de contenu pour les objets de SPIP',
);
